# ~/.bashrc

export TZ='Europe/Kiev'
#export TZ='Europe/Vienna';
export PAGER=less
# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias pkill='pkill -e'
alias ls='ls --color=auto'
alias la='ls -a'
alias sudo='sudo -E '
alias grep='grep --color=auto'
cd() { builtin cd "$@" && ls; }
alias bb='bb --classpath .:src'
alias view='less -R'
alias ipinfo='curl ipinfo.io'
alias vim='/bin/nvim'
alias gs='git status'

ide()
{
    if [ -e "Session.vim" ]
    then
      ProjectDir=$(pwd) vim -S $@ 
    else
      ProjectDir=$(pwd) vim $@
    fi
}

sudo loadkeys ~/.config/dotfiles/keyremap;

export PATH="${PATH}:${HOME}/.local/bin"
export VISUAL=nvim

HISTSIZE=20000
HISTFILESIZE=$HISTSIZE
HISTCONTROL=erasedups
HISTIGNORE="?:??:exit:ide:startx:cd ..:vim:git status"
shopt -s histappend 

red="\[\e[0;31m\]"
green="\[\e[0;32m\]"
yellow="\[\e[0;33m\]"
blue="\[\e[0;34m\]"
purple="\[\e[0;35m\]"
cyan="\[\e[0;36m\]"
white="\[\e[0;37;1m\]"
orange="\[\e[0;91m\]"
reset="\[\e[39m\]"

PS1="${green}-> \w\n${cyan}\u@\h${reset}\$ "

if [ -f ~/.git-prompt.sh ]; 
then
	. ~/.git-prompt.sh
	GIT_PS1_SHOWDIRTYSTATE=true
	GIT_PS1_SHOWCOLORHINTS=true
	GIT_PS1_UNTRACKEDFILES=true
	#PROMPT_COMMAND="__git_ps1 '\u@\h:\w' '\\$ '"
	PROMPT_COMMAND="__git_ps1 '${green}-> \w' '\n${cyan}\u@\h${reset}\$ '"
fi
#set auto complete after sudo 
complete -cf sudo
#disable ctrl+s blocking on terminal
stty -ixon
#disable ctrl+d exit shortcut 
set -o ignoreeof
source /usr/share/fzf/key-bindings.bash
source /usr/share/fzf/completion.bash
export FZF_DEFAULT_OPTS='--bind=ctrl-j:down,ctrl-k:up'
export FZF_COMPLETION_TRIGGER='..'
source /usr/share/bash-completion/completions/git
source /usr/share/bash-completion/completions/docker

sudo asusctl -k off
set show-all-if-ambiguous on
shopt -s extglob
