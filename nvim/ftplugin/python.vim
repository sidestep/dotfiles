set number
nmap <leader>er :w<CR>G:read !python %<CR>
map <leader>eb :w<CR>:FloatermNew --wintype=vsplit --autoclose=0 python %<CR>
