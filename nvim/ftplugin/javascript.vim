set number

nmap <leader>er :FloatermNew --wintype=vsplit --autoclose=0 deno repl -A --eval="const mod = await import('%:p')"<CR>
nmap <leader>eb :w<CR>:FloatermNew --wintype=vsplit --autoclose=0 deno run -A %<CR> 
nmap <leader>et :w<CR>:FloatermNew --wintype=vsplit --autoclose=0 deno test -A $ProjectDir --reporter=dot \| sed 's/file:\/\///'<CR>

"release as in rollup
"nmap <leader>rr :w<CR>:FloatermNew --wintype=vsplit --autoclose=0 npx rollup app.js --file ../js/app.js<CR>
nmap <leader>rr :w<CR>:FloatermNew --wintype=vsplit --autoclose=0 npx rollup '%:p' --file $(sed 's/\/src/\/js/i' <<< '%:p')<CR>

nnoremap <Leader>l "ayiwoconsole.log(<C-R>a); //!--- delete me!<Esc>
vnoremap <Leader>l "ayoconsole.log(<C-R>a); //!--- delete me!<Esc>
