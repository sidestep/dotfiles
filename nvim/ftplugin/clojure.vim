set number

let g:sexp_enable_insert_mode_mappings="false"

let g:conjure#client#clojure#nrepl#connection#auto_repl#cmd="clojure -Mnrepl -p 8794"
let g:conjure#log#wrap="true"
let g:conjure#log#hud#height=0.5
let g:conjure#log#break_length=70
let g:conjure#highlight#enabled="true"
let g:conjure#highlight#timeout=200
let g:conjure#highlight#group="Visual"

set sessionoptions=options,tabpages

"to write and close all buffers, including term buffer opened by conjure, on :xa
autocmd TermOpen * autocmd BufWriteCmd <buffer> :

"to hide 'nrepl auto open terminal' from buff list
autocmd TermOpen * set nobuflisted
