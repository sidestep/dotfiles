vim.cmd([[source ~/.config/nvim/vimrc]])

require('config.lsp')
require('config.cmp')
require('config.status_line')
require('nvim-autopairs').setup()
require('nvim-treesitter.configs').setup( 
{
  ensure_installed = { "python", "javascript" },
  indent = { enable = false },
  highlight = { enable = false, additional_vim_regex_highlighting = false },
})
vim.diagnostic.config(
{
  float = {scope='cursor'},
  severity_sort = false,
  signs = false,
  underline = true,
  update_in_insert = false,
  virtual_text = false
})

function project_root()
    local root_patterns = { "deno.json", ".git" }
    return vim.fs.dirname(vim.fs.find(root_patterns, { upward = true })[1])
end

--fzf lua
fzfLua = require('fzf-lua')
fzfDir = os.getenv('ProjectDir') or '.' 
fzfLua.setup({fzf_colors = true})
vim.keymap.set('n', '<localleader>f', function() fzfLua.files({cwd = fzfDir}) end)
vim.keymap.set('n', '<localleader>F', function() fzfLua.files({cwd = project_root() or '..'}) end) 
vim.keymap.set('n', '<localleader>g', function() fzfLua.grep_project({cwd = fzfDir}) end) 
vim.keymap.set('n', '<localleader>h', fzfLua.oldfiles) 
vim.keymap.set('n', '<localleader>cl', fzfLua.lsp_references) 
vim.keymap.set('n', '<localleader>ca', fzfLua.lsp_code_actions) 

vim.keymap.set('n', '<C-j>', vim.cmd.bprev) 
vim.keymap.set('n', '<C-k>', vim.cmd.bnext) 
-- gt goes no next buffer if no tab opened
vim.keymap.set('n', '<leader>x', vim.cmd.bdelete)
vim.keymap.set('n', 'gt', 
function() 
    if(vim.fn.tabpagenr('$') > 1) then
        vim.cmd.tabnext()
    else
        vim.cmd.bnext()
    end
end) 

vim.api.nvim_create_autocmd('textyankpost', { callback = function() vim.highlight.on_yank({higroup="IncSearch", timeout=200}) end })

