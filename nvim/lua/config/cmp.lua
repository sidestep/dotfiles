local cmp = require('cmp')

local snippet = {
      expand = function(args)
        require('luasnip').lsp_expand(args.body)
      end
    }

local sources = cmp.config.sources(
{
      --{ name = 'nvim_lsp', priority = 1, keyword_length = 1 },
      --{ name = 'luasnip' },
      { name = 'buffer', keyword_length=2 },
      { name = 'nvim_lsp', keyword_length=2 },
      { name = 'path', keyword_length=2 },
      { name = 'nvim_lsp_signature_help' }
})

local has_words_before = function()
  unpack = unpack or table.unpack
  local line, col = unpack(vim.api.nvim_win_get_cursor(0))
  return col ~= 0 and vim.api.nvim_buf_get_lines(0, line - 1, line, true)[1]:sub(col, col):match("%s") == nil
end

local go_next = cmp.mapping(
     function(fallback)
        if cmp.visible() then
            cmp.select_next_item()
        elseif has_words_before() then
            cmp.complete()
          else fallback() -- The fallback function sends a already mapped key. In this case, it's probably `<Tab>`.
        end
      end, 
      {'i'})

local go_prev = cmp.mapping(function() if cmp.visible() then cmp.select_prev_item() end end, { 'i' })

local mapping = 
{
    ["<Tab>"] = go_next,
    ["<C-j>"] = go_next,
    ["<C-k>"] = go_prev,
    ['<C-c>'] = cmp.mapping.abort(),
    ['<CR>']  = cmp.mapping.confirm({ behavior = cmp.ConfirmBehavior.Insert, select = true }), 
}

formatting = {
    format = function(entry, vim_item)
      -- Kind icons
      -- vim_item.kind = string.format('%s %s', kind_icons[vim_item.kind], vim_item.kind) -- This concatonates the icons with the name of the item kind
      -- Source
      vim_item.menu = ({
        buffer = "[Buf]",
        nvim_lsp = "[LSP]",
        path = "[Pat]"
        --luasnip = "[LuaSnip]",
        --nvim_lua = "[Lua]",
        --latex_symbols = "[LaTeX]",
      })[entry.source.name]
      return vim_item
    end
  }

cmp.setup(
{
    snippet = snippet,
    mapping = mapping,
    sources = sources,
    formatting = formatting,
    completion = { completeopt = 'menu,menuone,preview' },
})

--require("luasnip.loaders.from_vscode").lazy_load({ include = { "html", "css" } }) 
--cmp.setup.filetype({'html', 'css'}, 
--{
--   sources = cmp.config.sources(
--   {
--     { name = 'luasnip', priority=0 },
--     { name = 'buffer' },
--     { name = 'path' }, 
--   })
--})

-- Use buffer source for `/` and `?` (if you enabled `native_menu`, this won't work anymore).
--cmp.setup.cmdline({ '/', '?' }, 
--{ 
--    mapping = mapping,
--    sources = { { name = 'buffer' } },
--})

-- Use cmdline & path source for ':' (if you enabled `native_menu`, this won't work anymore).
--cmp.setup.cmdline(':', 
--{ 
--    mapping = mapping, 
--    enabled = false,
--    --enabled = function() return not vim.tbl_contains({'w', 'x', 'xa', '!'}, vim.fn.getcmdline()) end,
--    sources = cmp.config.sources( 
--    { 
--        { name = 'path' }, 
--        { name = 'cmdline' }, 
--    })
--})
--
local cmp_autopairs = require('nvim-autopairs.completion.cmp')
cmp.event:on('confirm_done', cmp_autopairs.on_confirm_done())
