local lspconfig = require('lspconfig')
local cmp_capabilities = require('cmp_nvim_lsp').default_capabilities()

local servers = { 'pyright', 'clojure_lsp', 'denols', 'html', 'cssls' }
for _, server in ipairs(servers) do
    lspconfig[server].setup{ capabilities = cmp_capabilities }
end

-- Global mappings.
-- See `:help vim.diagnostic.*` for documentation on any of the below functions
--vim.keymap.set('n', '<loacalleader>df', vim.diagnostic.open_float)
--vim.keymap.set('n', '[d', vim.diagnostic.goto_prev)
--vim.keymap.set('n', ']d', vim.diagnostic.goto_next)
--vim.keymap.set('n', '<localleader>dq', vim.diagnostic.setloclist)

-- Use LspAttach autocommand to only map the following keys
-- after the language server attaches to the current buffer


vim.api.nvim_create_autocmd('LspAttach', 
{
  group = vim.api.nvim_create_augroup('UserLspConfig', {}),
  callback = function(ev)
    local client = vim.lsp.get_client_by_id(ev.data.client_id)
    -- Buffer local mappings.
    -- See `:help vim.lsp.*` for documentation on any of the below functions
    local opts = { buffer = ev.buf }
    vim.keymap.set('n', '<localleader>cd', vim.lsp.buf.implementation, opts)
    vim.keymap.set('n', '<localleader>cr', vim.lsp.buf.rename, opts)
    --vim.keymap.set('n', '<localleader>ca', vim.lsp.buf.code_action, opts)
    --vim.keymap.set('n', '<localleader>cl', vim.lsp.buf.references, opts)
    vim.keymap.set('n', '<localleader>ch', vim.lsp.buf.signature_help, opts)

    
    if client.server_capabilities.documentHighlightProvider then
        vim.opt.updatetime = 500
        local ag = vim.api.nvim_create_augroup('lsp_document_highlight', {clear=true})
        vim.api.nvim_create_autocmd({ 'CursorHold' }, -- 'CursorHoldI' },
            { 
                group = ag, 
                callback = function()
                    vim.lsp.buf.document_highlight()
                    vim.diagnostic.open_float(nil, {focus=false, scope="cursor", header=''})
                end
            }) 
        vim.api.nvim_create_autocmd({ 'CursorMoved', 'InsertEnter' },
            { group = ag, callback = vim.lsp.buf.clear_references, })
    end
    --vim.keymap.set('n', '<localleader>cf', vim.lsp.buf.format, opts)
    --vim.keymap.set('n', '<localleader>lgD', vim.lsp.buf.declaration, opts)
    --vim.keymap.set('n', '<localleader>lgd', vim.lsp.buf.definition, opts)
    --vim.keymap.set('n', '<localleader>lwa', vim.lsp.buf.add_workspace_folder, opts)
    --vim.keymap.set('n', '<localleader>lwr', vim.lsp.buf.remove_workspace_folder, opts)
    --vim.keymap.set('n', '<localleader>lwl', function()
    --  print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
    --end, opts)
    --vim.keymap.set('n', '<localleader>lD', vim.lsp.buf.type_definition, opts)
    --vim.keymap.set('n', '<localleader>la', vim.lsp.buf.code_action, opts)
    --vim.keymap.set('n', '<localleader>lgr', vim.lsp.buf.references, opts)
    --end, opts)
  end,
})
