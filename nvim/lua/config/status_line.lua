function set_status(active_buf)
    local buffs = {}
    local no_list_buffs = {['']=true, ['terminal']=true}
    for _,v in pairs(vim.fn.getbufinfo({buflisted=1})) do
            local fname = vim.fn.fnamemodify(v.name, ":t")
            if(not no_list_buffs[fname]) then 
                if(v.bufnr == active_buf) then
                    fname = '%#CursorLine#'..fname..'%*'
                end
                table.insert(buffs, fname)
            end
    end
    vim.o.statusline = '%F%m %y%= %c,%l/%L %P <'..table.concat(buffs, ',')..'>'
end

local g = vim.api.nvim_create_augroup('BufStatusLine', {}), 

vim.api.nvim_create_autocmd({'VimEnter'},
    { 
        group = g,
        callback = function(args)
            local buf = vim.api.nvim_create_buf(false, true)
            local win = vim.api.nvim_open_win(buf, false, 
            { 
                relative='editor',
                width=1,
                height=1,
                anchor='NE',
                row=0,
                col=vim.o.columns,
                focusable=false,style='minimal',
                noautocmd=true,
            }) 
            local show_buffs = function(args)
                    fname = vim.fn.fnamemodify(args.file, ":t")
                    set_status(args.buf)
                    vim.api.nvim_win_set_width(win, string.len(fname))
                    vim.api.nvim_buf_set_lines(buf, 0, 1, false, {fname}) 
                end
            vim.api.nvim_create_autocmd({'BufEnter'},
            { 
                group = g,
                callback = show_buffs 
            })
            show_buffs(args)
        end
    })
